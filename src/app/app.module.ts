import { ContactItem } from './../components/contact-item/contact-item';
import { SelectAvatar } from './../components/select-avatar/select-avatar'

import { ViewContactPage } from './../pages/view-contact/view-contact';
import { AddContactPage } from './../pages/add-contact/add-contact';
import { EditContactPage } from './../pages/edit-contact/edit-contact';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home'

import { CapitalizePipe } from '../pipes/CapitalPipe'

import { ContactProvider } from '../providers/contact'

@NgModule({
  declarations: [
    MyApp,
    AddContactPage,
    EditContactPage,
    HomePage,
    ViewContactPage,


    //pipes
    CapitalizePipe,

    //components
    ContactItem,
    SelectAvatar
    
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    EditContactPage,
    AddContactPage,
    ViewContactPage
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ContactProvider,
  ]
})
export class AppModule {}
