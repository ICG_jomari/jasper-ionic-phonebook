import { IContactUser } from './contact';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the Contacts provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ContactProvider {

  private users : IContactUser[]  = []

  constructor(public http: Http) {
    let users = localStorage.getItem("users")
    this.users = JSON.parse(users? users : "[]")
  }

  getUsers(index? : number) : any {
    if(typeof index != 'undefined') return this.users[index]
    return this.users
  }

  addUser(user : IContactUser){
    this.users.push(user)
    this.saveToStorage()
  }

  editUser(key : number, user : IContactUser){
    //[name, phoneNumber, avatar]
    this.users[key] = user;

    this.saveToStorage()
  }

  saveToStorage(){
    localStorage.setItem("users", JSON.stringify(this.users))
  }


}


export interface IContactUser{
  name : string,
  phoneNumber : string,
  avatar : string,
}
