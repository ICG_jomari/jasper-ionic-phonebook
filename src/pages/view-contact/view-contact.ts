import { IContactUser, ContactProvider } from './../../providers/contact';
import { EditContactPage } from './../edit-contact/edit-contact';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/*
  Generated class for the ViewContact page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-view-contact',
  templateUrl: 'view-contact.html'
})
export class ViewContactPage {

  user : IContactUser;
  index : number
  constructor(public navCtrl: NavController, public navParams: NavParams, public contactProvider : ContactProvider) {
    this.index = this.navParams.get('index')
    this.ionViewWillEnter()
  }

  ionViewWillEnter(){
    console.log("entered")
    this.user = this.contactProvider.getUsers(this.index)
  }

  edit(){
    let { user, index } = this
    this.navCtrl.push(EditContactPage, {user, index})
  }
}
