import { ContactProvider, IContactUser } from './../../providers/contact';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/*
  Generated class for the EditContact page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-edit-contact',
  templateUrl: 'edit-contact.html'
})
export class EditContactPage {
  


  contact: IContactUser = {
    name : "",
    avatar : "",
    phoneNumber : ""
  }

  index : number;
  constructor(public navCtrl: NavController, public navParams: NavParams, public contactProvider : ContactProvider) {
    this.index = this.navParams.get('index')
    let contact = this.contactProvider.getUsers(this.index)
    this.contact = {
       name : contact.name,
       phoneNumber : contact.phoneNumber,
       avatar : contact.avatar
    }
    
  }



  update(){
    this.contactProvider.editUser(this.index, this.contact)
    this.navCtrl.pop()
  }
  
  selectAvatar(avatar){
    this.contact.avatar = avatar;
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad EditContactPage');
  }

}
