import { IContactUser, ContactProvider } from './../../providers/contact';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/*
  Generated class for the AddContact page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-add-contact',
  templateUrl: 'add-contact.html'
})
export class AddContactPage {

  contact: IContactUser = {
    name : "",
    phoneNumber : "",
    avatar : ""
  };

  constructor(public navCtrl: NavController, public contactProvider : ContactProvider) {}

  addContact(){
    this.contactProvider.addUser(this.contact)
    this.navCtrl.pop()
  }

}
