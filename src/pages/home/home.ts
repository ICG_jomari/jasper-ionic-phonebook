import { ViewContactPage } from './../view-contact/view-contact';
import { AddContactPage } from './../add-contact/add-contact';

import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

import { ContactProvider, IContactUser } from '../../providers/contact'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  contacts : IContactUser[] = []
  counter : number = 0;
  constructor(public navCtrl: NavController, private contactProvider : ContactProvider) {
    this.contacts = this.contactProvider.getUsers()
  }

  search(text:string){

  }

  goToAddPage(){
    this.navCtrl.push(AddContactPage)
  }

  test($event:any){
    $event.stopPropagation()
  }


  viewContact(index : number, name : string){
    console.log(this.counter, "Home Page")
    let user = this.contacts[index]
    this.navCtrl.push(ViewContactPage, {index, user})
  }

}
