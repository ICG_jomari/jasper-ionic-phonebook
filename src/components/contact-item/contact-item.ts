import {Component, Input, Output, EventEmitter} from "@angular/core"

@Component({
    selector : 'contact-item',
    templateUrl : 'contact-item.html'
})
export class ContactItem {
    @Input() user
    @Input() counter
    @Output() userClicked = new EventEmitter()
    @Output() counterChange = new EventEmitter()

    itemClicked(){
        this.counter++;
        console.log(this.counter, "Im in Contact Item")
        this.counterChange.emit(this.counter)
        this.userClicked.emit(this.user.name)
    }
}