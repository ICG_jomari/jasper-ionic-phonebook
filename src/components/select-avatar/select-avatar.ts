import { EventEmitter } from '@angular/core';
import { Output } from '@angular/core';
import { Input } from '@angular/core';
import { Component } from '@angular/core';

@Component({
    selector: 'select-avatar',
    templateUrl: 'select-avatar.html'
})
export class SelectAvatar{
    @Input() avatar
    @Output() avatarChange = new EventEmitter()

    private avatarList = [
        "assets/images/image-1.jpg",
        "assets/images/image-2.jpg",
        "assets/images/image-3.jpg",
        "assets/images/image-4.jpg",
        "assets/images/image-6.jpg",
        "assets/images/image-7.jpg",
        "assets/images/image-8.jpg",
        "assets/images/image-9.jpg"
    ]

    switchAvatar(avatar){
        this.avatar = avatar
        this.avatarChange.emit(this.avatar)
    }

}