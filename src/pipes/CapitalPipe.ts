import {Pipe, PipeTransform} from '@angular/core'

@Pipe({
    name : "capitalize"
})
export class CapitalizePipe implements PipeTransform{
    transform(value : string, additional : string = ''){
        let lowercasedWordArray = value.toLowerCase().split('')
        lowercasedWordArray[0] = lowercasedWordArray[0].toUpperCase() 
        return lowercasedWordArray.join('') + additional;
    }
}

// 905199 ...  +6305199
// 0905199 ... +6305
// +639   ...  +63905
// else    ... 0